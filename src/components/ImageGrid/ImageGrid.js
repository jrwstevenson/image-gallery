import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { loadImages } from "../../actions";
import Stats from "../Stats";

import {
  Button,
  withStyles,
  Grid,
  GridListTileBar,
  IconButton
} from "@material-ui/core";
import CloudDownload from "@material-ui/icons/CloudDownload";
import Visibility from "@material-ui/icons/Visibility";
import "./styles.css";

const styles = theme => ({
  titleBar: {
    background:
      "linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, " +
      "rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)"
  },
  icon: {
    color: "white"
  },
  a: {
    color: "white"
  }
});

class AdvancedGridList extends Component {
  componentDidMount() {
    this.props.loadImages();
  }

  render() {
    const {
      classes,
      images,
      error,
      isLoading,
      loadImages,
      imageStats
    } = this.props;
    return (
      <div className="content">
        <Grid className="grid">
          {images.map(image => (
            <Grid
              item
              className={`item item-${Math.ceil(image.height / image.width)}`}
              key={image.id}
            >
              <img src={image.urls.small} alt={image.user.username} />
              <GridListTileBar
                title={image.title}
                titlePosition="top"
                actionIcon={
                  <div>
                    <Stats className="chip" stats={imageStats[image.id]} />
                    <IconButton>
                      <a
                        title="View"
                        className={classes.icon}
                        href={image.links.html}
                        target="_blank"
                      >
                        <Visibility />
                      </a>
                    </IconButton>
                    <IconButton>
                      <a
                        title="Download"
                        className={classes.icon}
                        href={`${image.links.download}?force=true`}
                        target="_blank"
                      >
                        <CloudDownload />
                      </a>
                    </IconButton>
                  </div>
                }
                actionPosition="left"
                className={classes.titleBar}
              />
            </Grid>
          ))}
        </Grid>
        {error && <div className="error">{JSON.stringify(error)}</div>}
        <Button onClick={() => !isLoading && loadImages()}>Load More</Button>
      </div>
    );
  }
}

AdvancedGridList.propTypes = {
  classes: PropTypes.object.isRequired,
  images: PropTypes.array.isRequired,
  error: PropTypes.string,
  isLoading: PropTypes.bool.isRequired,
  loadImages: PropTypes.func.isRequired,
  imageStats: PropTypes.object.isRequired
};

const mapStateToProps = ({ isLoading, images, error, imageStats }) => ({
  isLoading,
  images,
  error,
  imageStats
});

const mapDispatchToProps = dispatch => ({
  loadImages: () => dispatch(loadImages())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(AdvancedGridList));
