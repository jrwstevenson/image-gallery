import React from "react";
import { Chip } from "@material-ui/core";

const content = stats => {
  if (!stats) {
    return "Loading...";
  } else if (stats.error) {
    return "😫 Error!";
  } else if (stats.isLoading) {
    return "Loading...";
  } else if (stats.downloads !== null) {
    return `${stats.downloads} Likes`;
  }
};

const Stats = data => {
  const { stats } = data;
  return (
    <Chip
      style={{ marginLeft: 10, backgroundColor: "white" }}
      label={content(stats)}
    />
  );
};

export default Stats;
