# Image Gallery

[View demo here](https://image-gallery.james-stevenson.com/)

This project uses [Create React App](https://github.com/facebook/create-react-app),
[Redux Saga](https://redux-saga.js.org/)
and
[Material-UI](https://material-ui.com/)

I hadn't used [Saga](https://redux-saga.js.org/) before so this is a very simple app that displays images using the [Unsplash API](https://unsplash.com/developers). Once the images are successfully loaded, another set of requests are sent to get the statistics for each image. Each image displays its number of likes, a button to view and another to download the image.

Note: The free Unsplash API will only accepts a [limited](https://unsplash.com/documentation#registering-your-application) number of requests per hour.

### Getting Started

1. Download or clone this repo
1. npm install
1. npm start
